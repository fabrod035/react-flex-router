import React from 'react';
import Home from './components/Home';
import Reacto from "./components/Reacto";
import Nativo from "./components/Nativo";
import Expo from "./components/Expo";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";



import './App.css';

function App() {

  return (
    <Router>
      <div className="wrapper">
        <nav>
          <ul>
            <li className="one">
              <Link to="/">Home</Link>
            </li>
            <li className="two">
              <Link to="/reacto">Reacto</Link>
            </li>
            <li className="three">
              <Link to="/nativo">Nativo</Link>
            </li>
            <li className="four">
              <Link to="/expo">Expo</Link>
            </li>
          </ul>
        </nav>
        <hr />

        <Switch>
          <Route exact path="/">
            <Home/>
          </Route>
          <Route path="/reacto">
            <Reacto />
          </Route>
          <Route path="/nativo">
            <Nativo />
          </Route>
          <Route path="/expo">
            <Expo />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
